

import requests
import os
import json

URL = "http://localhost:3333/api/films"
OUTPATH = "frontend/public/data/films.json"

data = requests.get(URL).json()

for film in data['films']:
    for key in '_watchlist', '_blocklist':
        if key in film:
            del film[key]

with open(OUTPATH, 'w') as f:
    json.dump(data, f)