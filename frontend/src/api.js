
const LOCAL_STORAGE_MODE = false

function copyChoices(source, dest, force=false) {
    source.forEach(function(s) {
        let match = dest.find((d) => d._id == s._id)
        if (match != undefined) {
            match._watchlist = s._watchlist;
            match._blocklist = s._blocklist;
        } else if (force) {
            dest.push({
                _id: s._id,
                _watchlist: s._watchlist,
                _blocklist: s._blocklist,
            })
        }
    })
}

async function getFilms() {
    let url = "api/films";
    if (LOCAL_STORAGE_MODE) {
        url = "data/films.json";
    }
    const res = await fetch(url);
    let results = await res.json();
    let films = results.films;
    let choices = getLocalChoices();

    if (LOCAL_STORAGE_MODE) {
        copyChoices(choices, films);
    } else {
        // export to localdata
        copyChoices(films, choices, true);
        saveLocalChoices(choices);
    }
    return films
}

async function saveChoice(choice, label) {
    if (!LOCAL_STORAGE_MODE) {
        let key = '_' + label;
        let method = choice[key] ? 'POST': 'DELETE';
        let url = 'api/films/' + choice._id + '/' + label;
        const res = await fetch(url, {'method': method});
    }
    let choices = getLocalChoices();
    copyChoices([choice], choices, true);
    saveLocalChoices(choices);
}

function getLocalChoices() {
    try {
        let choices = JSON.parse(window.localStorage.getItem('choices'));
        if (choices == null) {
            return [];
        }
        return choices;
    } catch {
        console.error('Exception while loading local choices')
        window.localStorage.setItem('choices', JSON.stringify(choices))
        return [];
    }
}

function saveLocalChoices(choices) {
    window.localStorage.setItem('choices', JSON.stringify(choices))
}

export { getFilms, saveChoice, LOCAL_STORAGE_MODE }

