const PurgeSvelte = require('purgecss-from-svelte');
const purgecss = require('@fullhuman/postcss-purgecss')({
  content: [
    './public/*.html',
    './src/**/*.svelte',
    './src/*.svelte',
  ],
  extractors: [
    {
      extractor: content => PurgeSvelte.extract(content),
      extensions: ["svelte"]
    }
  ],
  whitelistPatterns: [/svelte-/]
});

const production = !process.env.ROLLUP_WATCH

module.exports = {
  plugins: [
    require('tailwindcss'),
    ...(production ? [purgecss] : [])
  ]
};
