

from lib import models
from lib.nziff import NZIFF
from lib import metadata
from lib import imagepool
import os

from pprint import pprint

models.setup('db.sqlite3')

nziff = NZIFF()
films = nziff.retrieve_films()

print(len(films), 'films found')
for film in films:
    nziff.expand_film(film)

for film in films:
    if 'imdb_id' in film:
        meta = {'imdb_id': film['imdb_id']}
    else:
        meta = metadata.search(film['title'], film['year'])
        # if meta is None:
        #     continue

    if meta:
        if 'imdb_id' in meta:
            more_meta = metadata.get(meta['imdb_id'])
            if more_meta is not None:
                meta.update(more_meta)
        film.update(meta)

    imagepool.cache_images(film)
    models.Film.create_or_update(film)
    print(film['title'], film['year'])