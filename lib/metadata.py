from .params import Params

import requests

from .settings import CACHE_SERVER, TMDB_IMAGE_BASE_URL

def search(title, year):
    params = Params({
        'query': title,
        'year': year,
        # 'ignore_cache': True,
    })
    url = CACHE_SERVER + '/search?' + str(params)
    response = requests.get(url)
    response.raise_for_status()
    results = response.json()
    if 'error' in results:
        print('Error while retrieving url ', url, ': ', results['error'])
        return None
    if len(results['results']) == 0:
        return None
    result = results['results'][0]

    GET_KEYS = {
        'vote_average': 'tmdb_rating',
        'vote_count': 'tmdb_rating_votes',
        'popularity': 'tmdb_popularity',
        'imdb_id': 'imdb_id',
        'id': 'tmdb_id',
        'overview': 'tmdb_overview',
    }
    cleaned = {GET_KEYS[k]: result[k] for k in GET_KEYS if result.get(k) not in [None, 'N/A']}
    GET_KEYS = {
        'poster_path': ('tmdb_poster_image', TMDB_IMAGE_BASE_URL + '/{}'),
        'backdrop_path': ('tmdb_backdrop_image', TMDB_IMAGE_BASE_URL + '/{}'),
        'imdb_id': ('imdb_url', 'https://www.imdb.com/title/{}/'),
        'id': ('tmdb_url', 'https://www.themoviedb.org/movie/{}'),
    }
    for oldkey in GET_KEYS:
        newkey, template = GET_KEYS[oldkey]
        if result.get(oldkey) not in [None, 'N/A']:
            cleaned[newkey] = template.format(result.get(oldkey))
    if 'tmdb_popularity' in cleaned:
        cleaned['tmdb_popularity'] = round(cleaned['tmdb_popularity'], 1)
    return cleaned

def get(imdb_id):
    params = Params({
        'imdb_id': imdb_id,
        'provider': 'omdb'
        # 'ignore_cache': True,
    })
    url = CACHE_SERVER + '/get?' + str(params)
    response = requests.get(url)
    result = response.json()

    if 'error' in result:
        print('Error while retrieving url ', url, ': ', result['error'])
        return None
    GET_KEYS = {
        'imdb_rating': 'imdb_rating',
        'imdb_votes': 'imdb_rating_votes',
    }
    cleaned = {GET_KEYS[k]: result[k] for k in GET_KEYS if result.get(k) is not None}
    GET_KEYS = {
        'awards': 'awards',
        'metascore': 'metascore_rating',
        'genre': 'genres',
        'actors': 'actors',
    }
    def is_empty_value(v):
        return v in [None, 'N/A', '', []] or (type(v) is list and v[0] == 'N/A')
    cleaned.update({GET_KEYS[k]: result[k] for k in GET_KEYS if not is_empty_value(result.get(k))})
    # print(result['provider'], result['cached'], list(result.keys()), '\n---', list(cleaned.keys()))

    params = Params({
        'imdb_id': imdb_id,
        'provider': 'tmdb',
        # 'ignore_cache': True,
    })
    url = CACHE_SERVER + '/get?' + str(params)
    response = requests.get(url)
    try:
        result = response.json()
    except:
        print("ERROR", url, response.status_code)
        return None

    GET_KEYS = {
        'vote_average': 'tmdb_rating',
        'vote_count': 'tmdb_rating_votes',
        'popularity': 'tmdb_popularity',
        'overview': 'tmdb_overview',
    }

    cleaned.update({GET_KEYS[k]: result[k] for k in GET_KEYS if not is_empty_value(result.get(k))})

    GET_KEYS = {
        'poster_path': ('tmdb_poster_image', TMDB_IMAGE_BASE_URL + '/{}'),
        'backdrop_path': ('tmdb_backdrop_image', TMDB_IMAGE_BASE_URL + '/{}'),
        'imdb_id': ('imdb_url', 'https://www.imdb.com/title/{}/'),
        'id': ('tmdb_url', 'https://www.themoviedb.org/movie/{}'),
    }
    for oldkey in GET_KEYS:
        newkey, template = GET_KEYS[oldkey]
        if result.get(oldkey) not in [None, 'N/A']:
            cleaned[newkey] = template.format(result.get(oldkey))
    if 'tmdb_popularity' in cleaned:
        cleaned['tmdb_popularity'] = round(cleaned['tmdb_popularity'], 1)
    print(result['provider'], result['cached'], list(result.keys()), '\n---', list(cleaned.keys()))
    return cleaned