
import { writable } from 'svelte/store';

export const showBlocklist = writable(1);
export const showWatchlist = writable(1);
export const showUntagged = writable(1);