
from .models import Cache
import requests
from bs4 import BeautifulSoup, Tag

from pprint import pprint
from .film import Film

def cached_request(url, expires_in=86400):
    response = Cache.lookup(url)
    if response is None:
        print('Getting page "{url}"'.format(url=url))
        response = requests.get(url)
        response.raise_for_status()
        response = response.text
        Cache.store(url, response, expires_in)
    return response


class NZIFF:
    LIST_URL = "https://www.nziff.co.nz/2021/wellington/films/title/"

    CERTIFICATION_MAPPING = {
        "16": "R16",
    }

    def retrieve_films(self):
        response = cached_request(self.LIST_URL)
        soup = BeautifulSoup(response, "lxml")
        films = []
        for film in soup.select('article.card.film-card'):
            data = {}
            data['category'] = film.get('data-category')
            data['nziff_image'] = 'https://www.nziff.co.nz' + film.find(itemprop='image')['src']
            data['nziff_url'] = 'https://www.nziff.co.nz' + film.find(itemprop='url')['href']
            data['title'] = film.find(itemprop='name')
            data['directors'] = film.find(itemprop='director')
            data['description'] = film.find(itemprop='description')
            alt_title = film.find(itemprop='alternativeTitle')
            if alt_title is not None:
                data['alternative_title'] = alt_title
            # cleanup
            # get text from tags
            for key, elem in data.items():
                if type(elem) is Tag:
                    data[key] = elem.get_text().strip()
            # split director names
            if data['directors'] is not None:
                data['directors'] = [d.strip() for d in data['directors'].split(',')]
            # temp restrictions
            # skip films with no director listed
            if data['directors'] is None:
                print('No director:', data['title'])
                continue
            films.append(Film(data))
        return films

    def expand_film(self, film):
        response = cached_request(film['nziff_url'])
        soup = BeautifulSoup(response, "lxml")
        meta = {}
        # get countries
        section = soup.find(class_='film-geo')
        delim = section.find(class_='delimiter')
        meta['countries'] = [el.get_text().strip() for el in delim.find_previous_siblings("a")]
        # get languages
        meta['languages'] = [el.get_text().strip() for el in section.find_all(itemprop='inLanguage')]
        # get year
        meta['year'] = soup.find(itemprop='copyrightYear').get_text().strip()
        # get certification
        section = soup.find(class_='film-rating')
        abbr = section.find('abbr')
        certification = abbr.get_text().strip().replace("VOD ", "")
        if certification in self.CERTIFICATION_MAPPING:
            certification = self.CERTIFICATION_MAPPING[certification]
        meta['certification'] = certification
        meta['certification_description'] = abbr['title']
        notes = section.find(class_='rating-notes')
        if notes is not None and certification != "TBC":
            notes = notes.get_text().strip()
            notes = notes[0].upper() + notes[1:]
            if notes.startswith('Documentary film exempt'):
                notes = 'Documentary film exempt'
            meta['certification_notes'] = notes
            # Documentary film exempt
        # get duration
        meta['duration'] = soup.find(class_='film-meta').find(itemprop='duration').get_text().strip()
        meta['duration'] = int(meta['duration'].split(' ')[0])
        # get trailer
        trailer = soup.find(lambda x: x.has_attr('data-trailer-id') and x.name == 'a')
        if trailer is not None:
            meta['trailer_url'] = trailer['href']
        # get session dates
        dates = soup.select('section.session-list .session-table .day .date')
        session_dates = []
        for date in dates:
            day = date.find(class_='d').get_text().strip()
            month = date.find(class_='M').get_text().strip()
            session_dates += [{'day': day, 'month': month}]
        meta['session_dates'] = session_dates
        # get url and imdb_id from letterboxd link
        letterboxd = soup.find('a', string="Letterboxd")
        if letterboxd is not None:
            url = letterboxd['href']
            meta['letterboxd_url'] = url
            if '/imdb/' in url:
                # extract the imdb_id from url format https://letterboxd.com/imdb/<imdb_id>/
                meta['imdb_id'] = url.rstrip('/').rsplit('/')[-1]

        for block in soup.find_all(class_='meta-block'):
            title = block.find(class_='title')
            if title is None:
                continue
            title = title.get_text().strip()
            key = None
            if title in ['Editor', 'Editors']:
                key = 'editors'
            if title in ['Producer', 'Producers']:
                key = 'producers'
            elif title in ['Photography', 'Screenplay']:
                key = title.lower()
            if key is None:
                continue
            items = [el.get_text().strip() for el in block.find_all(itemprop='name')]
            if len(items) > 0:
                meta[key] = items
        film.update(meta)
