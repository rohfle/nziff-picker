module.exports = {
  theme: {
    extend: {
      height: {
        '72': '18rem',
        '80': '20rem'
      },
      minWidth: {
        '1/4': '25%',
        '1/2': '50%',
        '1/3': '33.3333%',
        '2/3': '66.6666%',
        '3/4': '75%'
      }
    }
  },
  variants: {},
  plugins: []
}
