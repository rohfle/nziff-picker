import requests
import os
import shutil

from urllib.parse import urlsplit, unquote_plus
from .settings import CACHE_DIR, CACHE_PATH

def cache_image(url, namespace=None):
    if namespace is not None:
        base_path = os.path.join(CACHE_DIR, namespace)
    else:
        base_path = CACHE_DIR

    if not os.path.exists(base_path):
        os.makedirs(base_path, exist_ok=True)

    result = urlsplit(url)

    filename = unquote_plus(result.path.rsplit('/', 1)[-1])
    new_url = CACHE_PATH + '/' + namespace + '/' + filename
    full_path = os.path.join(base_path, filename)

    if os.path.exists(full_path):
        # file already on disk, skip
        return new_url

    print('Downloading image "{url}"...'.format(url=url))
    r = requests.get(url, stream=True)
    if r.ok:
        with open(full_path, 'wb') as f:
            r.raw.decode_content = True
            shutil.copyfileobj(r.raw, f)
    else:
        print('Error while downloading image "{url}"'.format(url=url))
    return new_url


def cache_images(film):
    for key in film:
        if not key.endswith('_image'):
            continue
        namespace = key.replace('_image', '')
        film[key] = cache_image(film[key], namespace=namespace)
    return film