# NZIFF picker

A personal project to wishlist or reject movies from the NZIFF 2020 catalog. Cross references other metadata sources like TheMovieDB. Uses local storage in the browser to keep your selections

## License

Uses images and metadata from TheMovieDB and NZIFF.co.nz which belongs to respective parties

Uses some icons from boxicons

Otherwise

MIT License
© Rohan Fletcher 2020

