
from bottle import route, run, request, response, get, post, delete
from bottle import static_file
import os

from lib.models import Film, select, db_session, commit
from lib import models

models.setup('db.sqlite3')

@route('/api/films')
def list_films():
    with db_session:
        films = []
        for item in select(f for f in Film).order_by(Film.title):
            film = dict(item.metadata)
            film['_id'] = item.id
            film['_watchlist'] = item.watchlist
            film['_blocklist'] = item.blocklist
            films += [film]
    return {'films': films}

@route('/api/films/<id:int>/watchlist', method=['GET', 'POST', 'DELETE'])
def watchlist_film(id):
    with db_session:
        film = Film.get(id=id)
        if film is None:
            response.status = 404
            return
        if request.method == 'GET':
            return {'watchlist': film.watchlist}
        elif request.method == 'POST':
            film.watchlist = True
            commit()
            response.status = 204
            return
        elif request.method == 'DELETE':
            film.watchlist = False
            commit()
            response.status = 204
            return

@route('/api/films/<id:int>/blocklist', method=['GET', 'POST', 'DELETE'])
def blocklist_film(id):
    with db_session:
        film = Film.get(id=id)
        if film is None:
            response.status = 404
            return
        if request.method == 'GET':
            return {'blocklist': film.blocklist}
        elif request.method == 'POST':
            film.blocklist = True
            commit()
            response.status = 204
            return
        elif request.method == 'DELETE':
            film.blocklist = False
            commit()
            response.status = 204
            return

@route('/hello')
def hello():
    return "Hello World!"

@route('/')
def send_index():
    return static_file('index.html', root=os.path.abspath('./public'))

@route('/<filename:path>')
def send_static(filename):
    return static_file(filename, root=os.path.abspath('./public'))

run(host='localhost', port=3333, debug=True)