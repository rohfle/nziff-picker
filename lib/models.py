from datetime import datetime, timezone, timedelta
import threading
import time
import sched
from pony.orm import *

from . import settings

db = Database()

def parse_datetime(value):
    return datetime.strptime('%Y-%m-%d %H:%M:%S')

class Film(db.Entity):
    updated_at = Required(datetime, sql_default='CURRENT_TIMESTAMP')
    title = Required(str)
    year = Required(str)
    url = Required(str)
    metadata = Required(Json)
    watchlist = Required(bool, sql_default='FALSE')
    blocklist = Required(bool, sql_default='FALSE')

    @classmethod
    @db_session(optimistic=False)
    def create_or_update(self, film):
        url = film['nziff_url']
        title = film['title']
        year = film['year']
        if year is None or year == "":
            year = "????"
        result = select(f for f in Film if f.url == url).first()
        if result is None:
            result = Film(url=url, title=title, year=year, metadata=film)
        else:
            result.set(metadata=film)
        commit()



class CacheCleanupThread(threading.Thread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.daemon = True
        self.scheduler = sched.scheduler(time.time, time.sleep)

    def cleanup_cache():
        CacheEntry.cleanup_expired()
        self.scheduler.enter(settings.CLEANUP_INTERVAL, 1, self.cleanup_cache)

    def run(self, *args, **kwargs):
        self.scheduler.enter(settings.CLEANUP_INTERVAL, 1, self.cleanup_cache)
        self.scheduler.run()


class Cache(db.Entity):
    cached_at = Required(datetime)
    expires_at = Optional(datetime)
    url = Required(str, unique=True)
    response = Required(str)

    @classmethod
    def cleanup_expired(cls):
        with db_session:
            now = datetime.now()
            delete(
                c for c in Cache if (
                    c.expires_at is not None and c.expires_at < now
                )
            )
            commit()

    @classmethod
    @db_session(optimistic=False)
    def lookup(cls, url):
        result = select(c for c in Cache if c.url == url).first()
        if result is None:
            return None
        expires_at = result.expires_at
        now = datetime.now()
        if (expires_at is None or expires_at >= now):
            return result.response
        else:
            result.delete()
            commit()

    @classmethod
    @db_session
    def store(cls, url, response, expires_in=None):
        select(c for c in Cache if c.url == url).delete(bulk=True)
        cached_at = datetime.now()
        expires_at = None
        if expires_in is not None:
            expires_at = cached_at + timedelta(seconds=expires_in)
        entry = Cache(cached_at=cached_at,
                      expires_at=expires_at,
                      url=url, response=response)
        commit()


def setup(path):
    db.bind('sqlite', path, create_db=True)
    db.generate_mapping(create_tables=True)
    cleanup_thread = CacheCleanupThread()
    cleanup_thread.start()