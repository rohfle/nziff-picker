
import requests
import textwrap
import sys
import glob
import re
import sys

from ansimarkup import parse

from pprint import pprint

def supports_color():
    """
    Returns True if the running system's terminal supports color, and False
    otherwise.
    """
    plat = sys.platform
    supported_platform = plat != 'Pocket PC' and (plat != 'win32' or
                                                  'ANSICON' in os.environ)
    # isatty is not always implemented, #6223.
    is_a_tty = hasattr(sys.stdout, 'isatty') and sys.stdout.isatty()
    return supported_platform and is_a_tty

template = """<b><i><lr>{title} ({year})</lr></i>    <lg>{imdb_rating}/10 ({imdb_rating_votes} votes)</lg>  <ly>{duration} mins</ly>
<lc>{certification}</lc>   <dim>{certification_notes}</dim>
{genres_joined}    {countries_joined}    {languages_joined}</b>
{directors_joined}  {photography_joined}

{nziff_url}

<dim>{description_wrapped}</dim>

<dim>{plot_wrapped}</dim>
"""

import re

def list_template_keys(template):
    return [m.group(0).strip('{}') for m in re.finditer('\{[^\}]+\}', template)]


from lib.models import Film, select, db_session
from lib import models

models.setup('db.sqlite3')

with db_session:
    select(f for f in Film).show()
    for item in select(f for f in Film).order_by(Film.title):
        film = dict(item.metadata)
        JOIN_KEYS = ['genres', 'countries', 'languages', 'editors', 'directors', 'photography', 'producers']
        for key in list_template_keys(template):
            if key.endswith('_joined'):
                key = key.rsplit('_', 1)[0]
            if key in JOIN_KEYS:
                if key not in film:
                    film[key + '_joined'] = 'N/A'
                else:
                    film[key + '_joined'] = ', '.join(film[key])
            elif key not in film:
                film[key] = 'N/A'
        film['description_wrapped'] = textwrap.fill(film['description'])
        film['plot_wrapped'] = textwrap.fill(film.get('plot', ''))
        try:
            print(parse(template).format(**film))
        except:
            print(film)
            raise
