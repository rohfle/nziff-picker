function capitalize(str) {
    if(typeof str === 'string') {
        return str.replace(/^\w/, c => c.toUpperCase());
    } else {
        return '';
    }
}

function preloadImage(src) {
    return new Promise(function(resolve) {
        let img = new Image()
        img.onload = resolve
        img.src = src
    })
}

export {capitalize, preloadImage}

