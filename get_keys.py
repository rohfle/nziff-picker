
import requests
import textwrap
import sys
import glob
import re
import sys

from lib.models import Film, select, db_session
from lib import models
from pprint import pprint
models.setup('db.sqlite3')

with db_session:
    keys = []
    for item in select(f for f in Film).order_by(Film.title):
        film = dict(item.metadata)
        keys += film.keys()

pprint(set(keys))