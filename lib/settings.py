CLEANUP_INTERVAL = 86400
CACHE_SERVER = 'http://synology.local:5050'
TMDB_IMAGE_BASE_URL = 'http://image.tmdb.org/t/p/w370_and_h556_bestv2'
CACHE_DIR = './public/cache/'
CACHE_PATH = 'cache'